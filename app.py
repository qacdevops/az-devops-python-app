from flask import Flask, render_template, redirect, url_for, request
import requests

app = Flask(__name__)
app.static_folder = 'static'

@app.route('/', methods = ["GET"])
def home():
    return '<h1>Hello World!</h1>'

if __name__ == '__main__':
	app.run(debug = True, port = 5000, host = '0.0.0.0')